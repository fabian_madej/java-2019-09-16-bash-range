#!/usr/bin/env bash

start=15
koniec=50

if((start<koniec)); then
    for((i=start;i<=koniec;i++)) #od mniejszej do wiekszej
    do
        echo "${i} "
    done
else
    for((i=start;i>=koniec;i--)) #od wiekszej do mniejszej
    do
        echo "${i} "
    done
fi 